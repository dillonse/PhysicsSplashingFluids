#include "PhysicsUtilities.h"
#include <glm\geometric.hpp>
#define epsilon 0.001
bool intersection::isPenetrating(Particle& p, glm::vec3& tpoint,glm::vec3& tnormal) {
	return (glm::dot((p.position - tpoint), tnormal) < epsilon)&&(glm::dot(tnormal,p.velocity)<epsilon);
}

glm::vec3 response::impulse(Particle& p,glm::vec3& normal,glm::vec3& tangent,float restitutionCoeff){
	float velocityMagnitude= glm::length(p.velocity);
	glm::vec3 velocityDirection = glm::normalize(tangent + normal*restitutionCoeff);
	float speed = velocityMagnitude;
	p.velocity = velocityDirection*speed;
	return p.velocity;
}

 glm::vec3 response::forces(Particle& p, glm::vec3& normal, float penetrationCoeff) {
	 while (0);
	 glm::vec3 res = -glm::dot(p.force_accumulator, normal)*normal;
	 return res;
}

void response::postProcessing(Particle& p, glm::vec3& position, glm::vec3& normal,float restitutionCoeff){
	 glm::vec3 deltaX = -(glm::dot((p.position - position),normal))*normal;
	 glm::vec3 deltaV = -(glm::dot(p.velocity, normal))*normal;
	 p.position += deltaX;
	 p.velocity += (1.0f + restitutionCoeff)*deltaV;
}