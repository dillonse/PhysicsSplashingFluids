#ifndef _VBO_H_
#define _VBO_H_
#include <glm/glm.hpp>
#include <vector>
#include "model.h"
namespace buffers {
	void Load(std::vector<glm::vec3>& vertices, std::vector<glm::vec3>& normals, std::vector<glm::vec2>& UVs,GLuint& verticesID, GLuint& normalsID,GLuint& uvsID);
	void Load(std::vector<std::vector<glm::vec3>*>& buffers, std::vector<GLuint>& IDs);
	void Load(std::vector<std::vector<glm::vec2>*>& buffers, std::vector<GLuint>& IDs);
	void Load(std::vector<glm::vec3> colors,GLuint& colorsID);
	void Load(model& mod);
	void Bind(GLuint vertices);
	void Bind(GLuint vertices, GLuint normals, GLuint UVs);
	void Bind(model& mod);
	void RenderTriangles(int numVertices);
	void RenderIndices(int numIndices);
}
#endif