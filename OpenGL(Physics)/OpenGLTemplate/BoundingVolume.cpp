#include "BoundingVolume.h"
#include "VBO.h"
#include <glm\geometric.hpp>
#include <glm\gtx\transform.hpp>

void BoundingSphere::Init(instance& inst) {
	//get the center of the sphere
	this->position = glm::vec3(0.0f);
	for (int i = 0; i < inst.mod->vertices.size(); i++) {
		this->position += inst.mod->vertices[i];
	}
	this->position /= inst.mod->vertices.size();
	//compute the radius as the maximum distance between the centoid and a vertex of the mesh
	radius = 0.0f;
	for (int i = 0; i < inst.mod->vertices.size(); i++) {
		float dist = glm::length(this->position -inst.mod->vertices[i]);
		radius = dist > radius ? dist : radius;
	}
	//scale the sphere vertices by the radius
	for (int i = 0; i < this->inst.mod->vertices.size(); i++) {
		this->inst.mod->vertices[i] = radius*this->inst.mod->vertices[i]+this->position;
	}
	this->bodyspacePosition = this->position;
}

void BoundingSphere::Render() {
	inst.transform = glm::scale(glm::mat4(1.0f), glm::vec3(radius));
	inst.transform[3] = glm::vec4(position,1.0f);
	inst.Render();
}

void OBB::Init(instance& inst) {
	//calculate the extends
	//find the avg of positions
	position = glm::vec3(0.0f);
	for (int i = 0; i < inst.mod->vertices.size(); i++) {
		position += inst.mod->vertices[i];
	}
	position /= inst.mod->vertices.size();
	//find the xyz maximums, assuming box is in orthogonal position on its bodyspace
	extends = glm::vec3(0, 0, 0);
	for (int i = 0; i < inst.mod->vertices.size(); i++) {
		float xdist = glm::abs(position.x - inst.mod->vertices[i].x);
		float ydist = glm::abs(position.y - inst.mod->vertices[i].y);
		float zdist = glm::abs(position.z - inst.mod->vertices[i].z);
		if (extends.x < xdist)extends.x = xdist;
		if (extends.y < ydist)extends.y = ydist;
		if (extends.z < zdist)extends.z = zdist;
	}
	//calculate the unique/symmetric face normals (should be only three)
	//iterate through the normals on the vertices
	float epsilon = 0.0001f;
	faceNormals.clear();
	for (int i = 0; i < this->inst.mod->normals.size(); i += 3) {
		glm::vec3 n = (this->inst.mod->normals[i] + this->inst.mod->normals[i + 1] + this->inst.mod->normals[i + 2])*0.333333f; //calc the face normal
		bool isDuplicateSymmetric = false;
		for (int j = 0; j < faceNormals.size(); j++) {
			int same = 0;
			int symmetric = 0;
			if (glm::abs(faceNormals[j].x - n.x) < epsilon)same++;
			if (glm::abs(faceNormals[j].y - n.y) < epsilon)same++;
			if (glm::abs(faceNormals[j].z - n.z) < epsilon)same++;
			if (glm::abs(faceNormals[j].x + n.x) < epsilon && glm::abs(n.x)>epsilon)symmetric++;
			if (glm::abs(faceNormals[j].y + n.y) < epsilon && glm::abs(n.y)>epsilon)symmetric++;
			if (glm::abs(faceNormals[j].z + n.z) < epsilon && glm::abs(n.z)>epsilon)symmetric++;
			if (same == 3 || (same == 2 && symmetric == 1)) {
				isDuplicateSymmetric = true;
				break;
			}
		}
		if (!isDuplicateSymmetric) {
			faceNormals.push_back(n);
		}
	}
	bodyspaceFaceNormals = std::vector<glm::vec3>(faceNormals);
	//scale the vertices by the extends
	for (int i = 0; i < this->inst.mod->vertices.size(); i++) {
		this->inst.mod->vertices[i].x = this->inst.mod->vertices[i].x*4.0f*extends.x;
		this->inst.mod->vertices[i].y = this->inst.mod->vertices[i].y*4.0f*extends.y;
		this->inst.mod->vertices[i].z = this->inst.mod->vertices[i].z*4.0f*extends.z;
	}
	bodyspacePosition = position;
}

void OBB::Render(glm::mat3& orientation) {
	inst.transform = glm::scale(glm::mat4(1.0f),extends*4.0f);
	inst.transform = glm::mat4(orientation)*inst.transform;
	inst.transform[3] = glm::vec4(position, 1.0f);
	inst.Render();
	inst.transform = glm::mat4(1.0f);
	inst.BindMaterial();
	for (int i = 0; i <faceNormals.size(); i++) {
		glLineWidth(0.1);
		glBegin(GL_LINES);
		float length = 1.0f;
		glVertex3f(this->position.x, this->position.y, this->position.z);
		glVertex3f(this->position.x + length*faceNormals[i].x*length, this->position.y + faceNormals[i].y*length,
			this->position.z + faceNormals[i].z*length);
		glEnd();
	}
}