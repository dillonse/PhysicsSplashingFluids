#include "material.h"
#include "global.h"

void material::BindTextures()
{
	int mapCounter = 0;
	if (ShaderID == -1)return;
	glUseProgram(ShaderID);
	if (DiffuseMapID != -1) {
		glActiveTexture(GL_TEXTURE0+mapCounter);
		glBindTexture(GL_TEXTURE_2D, DiffuseMapID);

		GLuint DiffuseMapLocation = glGetUniformLocation(ShaderID, "diffuseMap");
		glUniform1i(DiffuseMapLocation, mapCounter++);
	}
	if (NormalMapID != -1) {
		glActiveTexture(GL_TEXTURE0 + mapCounter);
		glBindTexture(GL_TEXTURE_2D, NormalMapID);
		GLuint NormalMapLocation = glGetUniformLocation(ShaderID, "normalMap");
		glUniform1i(NormalMapLocation, mapCounter++);
	}
	if (SpecularMapID != -1) {
		glActiveTexture(GL_TEXTURE0 + mapCounter);
		glBindTexture(GL_TEXTURE_2D, SpecularMapID);
		GLuint SpecularMapLocation = glGetUniformLocation(ShaderID, "specularMap");
		glUniform1i(SpecularMapLocation, mapCounter++);
	}

	if (lights::light0.map.depthMapID != -1) {
		glActiveTexture(GL_TEXTURE0 + mapCounter);
		glBindTexture(GL_TEXTURE_2D, lights::light0.map.depthMapID);
		GLuint depthMapLocation = glGetUniformLocation(ShaderID, "depthMap");
		glUniform1i(depthMapLocation, mapCounter++);
	}

	if (lights::light0.map.depthMapID != -1) {
		glActiveTexture(GL_TEXTURE0 + mapCounter);
		glBindTexture(GL_TEXTURE_2D, lights::light0.map.backfaceDepthMapID);
		GLuint backfaceDepthMap = glGetUniformLocation(ShaderID, "backfaceDepthMap");
		glUniform1i(backfaceDepthMap, mapCounter++);
	}

	if (lights::light0.map.colorMapID != -1) {
		glActiveTexture(GL_TEXTURE0 + mapCounter);
		glBindTexture(GL_TEXTURE_2D, lights::light0.map.colorMapID);
		GLuint colorMapLocation = glGetUniformLocation(ShaderID, "colorMap");
		glUniform1i(colorMapLocation, mapCounter++);
	}

	if (SkyboxMapID != -1) {
		glActiveTexture(GL_TEXTURE0+mapCounter);
		glBindTexture(GL_TEXTURE_CUBE_MAP, SkyboxMapID);
		GLuint SkyboxMapLocation = glGetUniformLocation(ShaderID,"skyboxMap");
		glUniform1i(SkyboxMapLocation, mapCounter++);
	}
}

void material::BindValues()
{
	GLuint AlphaValueLocation = glGetUniformLocation(ShaderID, "alphaValue");
	glUniform1f(AlphaValueLocation, AlphaValue);

	GLuint LPLocation = glGetUniformLocation(ShaderID, "LP");
	glUniform3f(LPLocation, lights::light0.position.x, lights::light0.position.y, lights::light0.position.z);

	GLuint lightSpaceMatrixLocation = glGetUniformLocation(ShaderID, "lightSpaceMatrix");
	glUniformMatrix4fv(lightSpaceMatrixLocation, 1, GL_FALSE, &(lights::light0.lightSpace[0][0]));

	GLuint lightViewLocation = glGetUniformLocation(ShaderID, "lightView");
	glUniformMatrix4fv(lightViewLocation, 1, GL_FALSE, &(lights::light0.lightView[0][0]));

	GLuint viewPosLocation = glGetUniformLocation(ShaderID, "viewPos");
	glUniform3f(viewPosLocation, camera::position.x, camera::position.y, camera::position.z);

	GLuint choiceLocation = glGetUniformLocation(ShaderID, "choice");
	glUniform1i(choiceLocation, choice);

	GLuint colorLocation = glGetUniformLocation(ShaderID, "color");
	glUniform4f(colorLocation,color.r,color.g,color.b,color.a);

}

void material::SwitchToLight() {
	ShaderID = lightShaderID;
}

void material::SwitchToObject() {
	ShaderID = objectShaderID;
}