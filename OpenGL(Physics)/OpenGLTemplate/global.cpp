#include "global.h"
#include<glm/gtc/matrix_transform.hpp>
//camera
glm::mat4 camera::ProjectionMatrix = glm::perspective(glm::radians(45.0f),0.5f, 0.1f, 100.0f); //Projection matrix
glm::mat4 camera::ViewMatrix = glm::mat4(1.0);
glm::mat4 camera::ViewMatrixNoTranslation= glm::mat4(1.0);
GLfloat camera::FOV = 60;
GLfloat camera::ratio = 0.5;
glm::vec3 camera::position = glm::vec3(0, 0, 0);
//viewport
int viewport::width = 10;
int viewport::height = 10;

//lights
light lights::light0;
light lights::light1;
//controls
GLboolean controls::Key_A = false;
GLboolean controls::Key_D = false;
GLboolean controls::Key_W = false;
GLboolean controls::Key_S = false ;
GLboolean controls::Key_Left = false;
GLboolean controls::Key_Right = false;
GLboolean controls::Key_I = false;
GLboolean controls::Key_J = false;
GLboolean controls::Key_K = false;
GLboolean controls::Key_L = false;
GLboolean controls::Key_U = false;
GLboolean controls::Key_O = false;
GLboolean controls::Key_0 = false;
GLboolean controls::Key_5 = false;
GLboolean controls::Key_8 = false;
GLboolean controls::Key_2 = false;
GLboolean controls::Key_4 = false;
GLboolean controls::Key_6 = false;
GLboolean controls::Key_7 = false;
GLboolean controls::Key_9 = false;
GLboolean controls::Key_1 = false;
GLboolean controls::Key_3 = false;
GLboolean controls::Key_plus = false;
GLboolean controls::Key_minus = false;
GLboolean controls::Key_G = false;
GLboolean controls::Key_times = false;
GLboolean controls::Key_divide = false;
GLboolean controls::MouseLeft = false;
GLboolean controls::MouseRight = false;
GLint controls::MousePositionX = 0;
GLint controls::MousePositionY = 0;
//time 
float time::currentFrameTime = 0;
float time::previousFrameTime = 0;
float time::deltaTime = 0;
float time::pastDeltaTime = 0;