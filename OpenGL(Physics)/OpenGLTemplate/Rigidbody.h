#pragma once
#include <glm\mat3x3.hpp>
#include "instance.h"
#include <vector>
#include "BoundingVolume.h"

#define DEBUG false
#define SPHERE 0
#define OBBS 1
#define SPHEREOBB 2
#define COLLISION SPHEREOBB
//resource for volume calculation http://stackoverflow.com/questions/1406029/how-to-calculate-the-volume-of-a-3d-mesh-object-the-surface-of-which-is-made-up
//Intetia tensor calculation resource http://www.melax.com/volint

class Rigidbody 
{
private:
	float CalculateVolume();
	glm::vec3 CalculateCOM();
	glm::mat3 CalculateInertia();
	
public:
	Rigidbody(instance& geometry,glm::vec3 position,instance& boundingSphere,instance& OBB);
	void Update(float deltaTime);
	void UpdateMomentums(float deltaTime);
	void UpdateAuxilary(float deltaTime);
	void FindNearestPoints(Rigidbody& collidee);
	void UpdateGeometry();
	void UpdateBoundingVolumes();
	void Render(material& debugMat,bool lines);
	void RenderDebug();
	void RenderLines(material& mat);
	void ClearNearestPoints();
	void ClearActives();
	instance geometry;
	model bodyspaceGeometry;
	float mass=10.0f;
	//rigidbody
	glm::mat3 bodyspaceInertia;
	glm::mat3 inertiaTensor=glm::mat3(1.0f);
	glm::vec3 position=glm::vec3(0.0f);
	glm::mat3 orientation=glm::mat3(1.0f);
	glm::vec3 linearMomentum=glm::vec3(0.0f);
	glm::vec3 angularMomentum=glm::vec3(0.0f);
	glm::vec3 netForce=glm::vec3(0,0,0);
	glm::vec3 netTorque=glm::vec3(0.0f);
	std::vector<glm::vec3> forces;
	std::vector<glm::vec3> nearestPoints;
	std::vector<glm::vec3> activeIndices;
	//bounding volumes
	BoundingSphere bsphere;
	OBB obb;
	//meta info
	bool isColliding = false;
};