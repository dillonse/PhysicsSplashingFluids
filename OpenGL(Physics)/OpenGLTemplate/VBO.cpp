#include <GL/glew.h>
#include "VBO.h"
void buffers::Load(std::vector<glm::vec3> &vertices, std::vector<glm::vec3>& normals, std::vector<glm::vec2>& UVs, GLuint& verticesID, GLuint& normalsID, GLuint& UVsID) {
	//generate the buffers
	glGenBuffers(1, &verticesID);                                  //generate the points buffer and return its id//
	glGenBuffers(1, &normalsID);
	glGenBuffers(1, &UVsID);
	//assign the data to buffers
	if (vertices.size() > 0) {
		glBindBuffer(GL_ARRAY_BUFFER, verticesID);                 //bind the buffer with the vbo id//
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(vertices[0]), &(vertices[0]), GL_STATIC_DRAW);//set the binded buffer with the points buffer//
	}
	if (normals.size()) {
		glBindBuffer(GL_ARRAY_BUFFER, normalsID);
		glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(normals[0]), &(normals[0]), GL_STATIC_DRAW);

	}
	if (UVs.size()) {
		glBindBuffer(GL_ARRAY_BUFFER, UVsID);
		glBufferData(GL_ARRAY_BUFFER, UVs.size() * sizeof(UVs[0]), &(UVs[0]), GL_STATIC_DRAW);
	}
}

void buffers::Load(std::vector<std::vector<glm::vec3>*>& buffers, std::vector<GLuint>& IDs) {
	//iterate over the buffers list
	IDs.resize(buffers.size());
	for (int i = 0; i < buffers.size(); i++) {
		if ((*buffers[i]).size() > 0) {
			glGenBuffers(1, &IDs[i]); //generate the ID
			glBindBuffer(GL_ARRAY_BUFFER, IDs[i]); //bind the buffer
			glBufferData(GL_ARRAY_BUFFER, (*buffers[i]).size() * sizeof(glm::vec3), &(*buffers[i])[0], GL_STATIC_DRAW); //attach the data
		}
	}
}
void buffers::Load(std::vector<std::vector<glm::vec2>*>& buffers, std::vector<GLuint>& IDs) {
	//iterate over the buffers list
	IDs.resize(buffers.size());
	for (int i = 0; i < buffers.size(); i++) {
		if ((*buffers[i]).size() > 0) {
			glGenBuffers(1, &IDs[i]); //generate the ID
			glBindBuffer(GL_ARRAY_BUFFER, IDs[i]); //bind the buffer
			glBufferData(GL_ARRAY_BUFFER, (*buffers[i]).size() * sizeof(glm::vec2), &(*buffers[i])[0], GL_STATIC_DRAW); //attach the data
		}
	}
}

void buffers::Load(model & mod)
{
	if (mod.vertices.size() > 0) {
		if(mod.verticesID==-1)
			glGenBuffers(1, &mod.verticesID);
		glBindBuffer(GL_ARRAY_BUFFER, mod.verticesID);
		glBufferData(GL_ARRAY_BUFFER, mod.vertices.size() * sizeof(glm::vec3), &mod.vertices[0], GL_STATIC_DRAW);
	}
	if (mod.normals.size() > 0) {
		if(mod.normalsID==-1)
			glGenBuffers(1, &mod.normalsID);
		glBindBuffer(GL_ARRAY_BUFFER, mod.normalsID);
		glBufferData(GL_ARRAY_BUFFER, mod.normals.size() * sizeof(glm::vec3), &mod.normals[0], GL_STATIC_DRAW);
	}
	if (mod.UVs.size() > 0) {
		if(mod.UVsID==-1)
			glGenBuffers(1, &mod.UVsID);
		glBindBuffer(GL_ARRAY_BUFFER, mod.UVsID);
		glBufferData(GL_ARRAY_BUFFER, mod.UVs.size() * sizeof(glm::vec2), &mod.UVs[0], GL_STATIC_DRAW);
	}
	if (mod.tangents.size() > 0) {
		if(mod.tangentsID==-1)
			glGenBuffers(1, &mod.tangentsID);
		glBindBuffer(GL_ARRAY_BUFFER, mod.tangentsID);
		glBufferData(GL_ARRAY_BUFFER, mod.tangents.size() * sizeof(glm::vec3), &mod.tangents[0], GL_STATIC_DRAW);
	}
	if (mod.bitangents.size() > 0) {
		if(mod.bitangetsID==-1)
			glGenBuffers(1, &mod.bitangetsID);
		glBindBuffer(GL_ARRAY_BUFFER, mod.bitangetsID);
		glBufferData(GL_ARRAY_BUFFER, mod.bitangents.size() * sizeof(glm::vec3), &mod.bitangents[0], GL_STATIC_DRAW);
	}

	if (mod.colors.size() > 0) {
		if(mod.colorsID==-1)
			glGenBuffers(1, &mod.colorsID);
		glBindBuffer(GL_ARRAY_BUFFER, mod.colorsID);
		glBufferData(GL_ARRAY_BUFFER, mod.colors.size() * sizeof(glm::vec3), &mod.colors[0], GL_STATIC_DRAW);
	}

	if (mod.indices.size() > 0) {
		glGenBuffers(1, &mod.indicesID);
		glBindBuffer(GL_ARRAY_BUFFER, mod.indicesID);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, mod.indices.size() * sizeof(glm::uvec3), &(mod.indices[0]), GL_STATIC_DRAW);
		//glBufferData(GL_ARRAY_BUFFER, mod.indices.size() * sizeof(glm::uvec3), &mod.indices[0], GL_STATIC_DRAW);
	}
	if (mod.actives.size() > 0) {
		if(mod.activesID==-1)
			glGenBuffers(1, &mod.activesID);
		glBindBuffer(GL_ARRAY_BUFFER, mod.activesID);
		glBufferData(GL_ARRAY_BUFFER, mod.actives.size() * sizeof(float), &(mod.actives[0]), GL_STATIC_DRAW);
		//glBufferData(GL_ARRAY_BUFFER, mod.indices.size() * sizeof(glm::uvec3), &mod.indices[0], GL_STATIC_DRAW);
	}
}

void buffers::Load(std::vector<glm::vec3> colors, GLuint& colorsID) {
	if (colors.size() > 0) {
		glGenBuffers(1,&colorsID);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER,colorsID);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,colors.size() * sizeof(glm::vec3), &colors[0], GL_STATIC_DRAW);
	}
}

void buffers::RenderTriangles(int verticesNum) {
	//render the vertices as a bunch of consecutive triangles
	glDrawArrays(GL_TRIANGLES, 0, verticesNum);
}

void buffers::RenderIndices(int indicesNum) {
	glDrawElements(GL_TRIANGLES,indicesNum, GL_UNSIGNED_INT, 0);
	//exit(100);
}

void buffers::Bind(GLuint verticesID) {
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, verticesID);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
}

void buffers::Bind(GLuint verticesID, GLuint normalsID, GLuint UVsID)
{
	//enable/disble the client states and(cond) bind the buffers and point 
	if (verticesID == -1) {
		glDisableClientState(GL_VERTEX_ARRAY);
	}
	else {
		glEnableClientState(GL_VERTEX_ARRAY);
		glBindBuffer(GL_ARRAY_BUFFER, verticesID);
		glVertexPointer(3, GL_FLOAT, 0, 0);
	}

	if (normalsID == -1) {
		glDisableClientState(GL_NORMAL_ARRAY);
	}
	else {
		glEnableClientState(GL_NORMAL_ARRAY);
		glBindBuffer(GL_ARRAY_BUFFER, normalsID);
		glNormalPointer(GL_FLOAT, 0, 0);
	}

	if (UVsID == -1) {
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}
	else {
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glBindBuffer(GL_ARRAY_BUFFER, UVsID);
		glTexCoordPointer(2, GL_FLOAT, 0, NULL);
	}
}

void buffers::Bind(model & mod)
{
	if (mod.verticesID == -1) {
		glDisableVertexArrayAttrib(0, 0);
	}
	else {
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, mod.verticesID);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}

	if (mod.normalsID == -1) {
		glDisableVertexArrayAttrib(1, 0);
	}
	else {
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, mod.normalsID);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}

	if (mod.UVsID == -1) {
		glDisableVertexArrayAttrib(2, 0);
	}
	else {
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, mod.UVsID);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}

	if (mod.tangentsID == -1) {
		glDisableVertexArrayAttrib(3, 0);
	}
	else {
		glEnableVertexAttribArray(3);
		glBindBuffer(GL_ARRAY_BUFFER, mod.tangentsID);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}

	if (mod.bitangetsID == -1) {
		glDisableVertexArrayAttrib(4, 0);
	}
	else {
		glEnableVertexAttribArray(4);
		glBindBuffer(GL_ARRAY_BUFFER, mod.bitangetsID);
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}

	if (mod.colorsID == -1) {
		glDisableVertexArrayAttrib(5, 0);
	}
	else {
		glEnableVertexAttribArray(5);
		glBindBuffer(GL_ARRAY_BUFFER, mod.colorsID);
		glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}

	
	if (mod.activesID == -1) {
		glDisableVertexArrayAttrib(6, 0);
	}
	else {
		glEnableVertexAttribArray(6);
		glBindBuffer(GL_ARRAY_BUFFER, mod.activesID);
		glVertexAttribPointer(6,1, GL_FLOAT, GL_FALSE, 0, (void*)0);
	}
}
