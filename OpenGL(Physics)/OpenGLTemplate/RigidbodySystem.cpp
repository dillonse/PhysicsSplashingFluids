#include "RigidbodySystem.h"
#include <iostream>
#include <algorithm>
#include <glm/gtx/string_cast.hpp>
#include "debug.h"
#include "Collider.h"
#include "Contact.h"
#include "global.h"

void RigidbodySystem::UpdateNearestPoints() {
	//find nearest points
	for (std::vector<Rigidbody>::iterator itt = rigidBodies.begin(); itt != rigidBodies.end(); itt++) {
		itt->ClearNearestPoints();
		itt->ClearActives();
		for (std::vector<Rigidbody>::iterator it = rigidBodies.begin(); it != rigidBodies.end()-1; it++) {
			if (itt != it)
				itt->FindNearestPoints(*it);
		}
	}
}

void RigidbodySystem::UpdateForcesTorque(float deltaTime) {
	for (std::vector<Rigidbody>::iterator itt = rigidBodies.begin(); itt != rigidBodies.end(); itt++) {
		itt->netForce = glm::vec3(0.0f);
		itt->netTorque = glm::vec3(0.0);
		//cleanup forces
		for (std::vector<glm::vec3>::iterator it = itt->forces.begin(); it != itt->forces.end(); it++) {
			*it = glm::vec3(0, 0, 0);
		}
		for (std::vector<UnaryForce*>::iterator it = unaryForces.begin(); it != unaryForces.end(); it++) {
			//if (itt->isColliding&&std::distance(unaryForces.begin(), it) == gravityIndex)continue;
			itt->netForce += (*it)->computeForce(*itt);
		}
		for (int i = 0; i < itt->geometry.mod->uniqueIndices.size(); i++) {
			itt->netTorque += glm::cross(itt->geometry.mod->vertices[itt->geometry.mod->uniqueIndices[i]] - itt->position, itt->forces[i]);
		}
		
		/*
		This code is for the narrow phase demo.
		//find nearest points
		itt->ClearNearestPoints();
		itt->ClearActives();
		for (std::vector<Rigidbody>::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++) {
			if(itt!=it)
			itt->FindNearestPoints(*it);
		}
		*/
	}
}

void RigidbodySystem::UpdateMomentums(float deltaTime)
{
	for (std::vector<Rigidbody>::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++) {
		if(!it->isColliding)
			it->UpdateMomentums(deltaTime);
	}
}

void RigidbodySystem::UpdateAuxilary(float deltaTime)
{
	for (std::vector<Rigidbody>::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++) {
		it->UpdateAuxilary(deltaTime);
	}
}

void RigidbodySystem::UpdateGeometry(float deltaTime)
{
	for (std::vector<Rigidbody>::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++) {
		it->UpdateGeometry();
	}
}

void RigidbodySystem::UpdateBoundingVolumes(float deltaTime)
{
	for (std::vector<Rigidbody>::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++) {
		it->UpdateBoundingVolumes();
	}
}

void RigidbodySystem::DetectCollisionBoundingSpheres() {
	broadPhaseCollisions.clear();
	for (std::vector<Rigidbody>::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++) {
		it->bsphere.inst.mat->color = glm::vec4(0, 1, 0, 0.5f);
	}
	for (std::vector<Rigidbody>::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++) {
		for (std::vector<Rigidbody>::iterator itt = it + 1; itt != rigidBodies.end(); itt++) {
			//check if the two bodies collide based on their bounding spheres

			if (detection::Sphere(*it,*itt)) {
				it->bsphere.inst.mat->color = glm::vec4(1, 0, 0, 0.5f);
				itt->bsphere.inst.mat->color = glm::vec4(1, 0, 0, 0.5f);
				//message += "\nCollision:" + glm::to_string(it->bsphere.inst.transform[3]) + " vs " + glm::to_string(itt->bsphere.inst.transform[3]);
				broadPhaseCollisions.push_back(std::make_pair(std::distance(rigidBodies.begin(),it),std::distance(rigidBodies.begin(),itt)));
			}
		}
	}
}

void RigidbodySystem::DetectCollisionOBBs() {
	broadPhaseCollisions.clear();
	for (std::vector<Rigidbody>::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++) {
		it->obb.inst.mat->color = glm::vec4(0, 1, 0, 0.5f);
	}
	for (std::vector<Rigidbody>::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++) {
		for (std::vector<Rigidbody>::iterator itt = it + 1; itt != rigidBodies.end(); itt++) {
			if (detection::OBB(*it, *itt)) {
				broadPhaseCollisions.push_back(std::make_pair(std::distance(rigidBodies.begin(), it), std::distance(rigidBodies.begin(), itt)));
				//message += "\nCollision:" + glm::to_string(it->position) + " vs " + glm::to_string(itt->position);
				it->obb.inst.mat->color = glm::vec4(1, 0, 0, 0.5f);
				itt->obb.inst.mat->color = glm::vec4(1, 0, 0, 0.5f);
			}
		}
	}
}

void RigidbodySystem::DetectCollisions() {
	broadPhaseCollisions.clear();
#if COLLISION == SPHERE
	for (std::vector<Rigidbody>::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++) {
		it->bsphere.inst.mat->color = glm::vec4(0, 1, 0, 0.5f);
	}
	for (std::vector<Rigidbody>::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++) {
		for (std::vector<Rigidbody>::iterator itt = it + 1; itt != rigidBodies.end(); itt++) {
			//use the sphere collision test as a proxy
			if (detection::Sphere(*it, *itt)) {
				it->bsphere.inst.mat->color = glm::vec4(1, 0, 0, 0.5f);
				itt->bsphere.inst.mat->color = glm::vec4(1, 0, 0, 0.5f);
				broadPhaseCollisions.push_back(std::make_pair(std::distance(rigidBodies.begin(), it), std::distance(rigidBodies.begin(), itt)));
				//message += "\nCollision:" + glm::to_string(it->position) + " vs " + glm::to_string(itt->position);
			}
		}
	}
#elif COLLISION == OBBS
	for (std::vector<Rigidbody>::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++) {
		it->obb.inst.mat->color = glm::vec4(0, 1, 0, 0.5f);
	}
	for (std::vector<Rigidbody>::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++) {
		for (std::vector<Rigidbody>::iterator itt = it + 1; itt != rigidBodies.end(); itt++) {
			//use the sphere collision test as a proxy
			if (detection::OBB(*it, *itt)) {
				broadPhaseCollisions.push_back(std::make_pair(std::distance(rigidBodies.begin(), it), std::distance(rigidBodies.begin(), itt)));
				//message += "\nCollision:" + glm::to_string(it->position) + " vs " + glm::to_string(itt->position);
				it->obb.inst.mat->color = glm::vec4(1, 0, 0, 0.5f);
				itt->obb.inst.mat->color = glm::vec4(1, 0, 0, 0.5f);
			}
		}
	}
#elif COLLISION == SPHEREOBB
	for (std::vector<Rigidbody>::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++) {
		it->bsphere.inst.mat->color = glm::vec4(0, 1, 0, 0.5f);
		it->obb.inst.mat->color = glm::vec4(0, 1, 0, 0.5f);
	}
	for (std::vector<Rigidbody>::iterator it = rigidBodies.begin(); it != rigidBodies.end(); it++) {
		for (std::vector<Rigidbody>::iterator itt = it + 1; itt != rigidBodies.end(); itt++) {
			//use the sphere collision test as a proxy
			if (detection::Sphere(*it, *itt)) {
				it->bsphere.inst.mat->color = glm::vec4(1, 0, 0, 0.5f);
				itt->bsphere.inst.mat->color = glm::vec4(1, 0, 0, 0.5f);
				if (detection::OBB(*it, *itt)) {
					broadPhaseCollisions.push_back(std::make_pair(std::distance(rigidBodies.begin(), it), std::distance(rigidBodies.begin(), itt)));
					//message += "\nCollision:" + glm::to_string(it->position) + " vs " + glm::to_string(itt->position);
					it->obb.inst.mat->color = glm::vec4(1, 0, 0, 0.5f);
					itt->obb.inst.mat->color = glm::vec4(1, 0, 0, 0.5f);
				}
			}
		}
	}
#endif
	

}

void RigidbodySystem::DetectContacts() {
	std::vector<Contact> finalContacts;
	contacts.clear();
	//iterate thhrough each rigidbody
	for (std::vector<Rigidbody>::iterator rigid_it = rigidBodies.begin(); rigid_it != rigidBodies.end(); rigid_it++) {
		//iterate through each plane
		//rigid_it->isColliding = false;
		rigid_it->obb.inst.mat->color = glm::vec4(0, 1, 0, 1);
		for (std::vector<Plane>::iterator plane_it = planes.begin(); plane_it != planes.end(); plane_it++) {
			//check for collisions between plane and rigidbody
			for (int i = 0; i < rigid_it->geometry.mod->uniqueIndices.size(); i++) {
				//check the distance between the plane and the vertice
				float dist = distance::point2plane(plane_it->position, plane_it->normal, rigid_it->geometry.mod->vertices[rigid_it->geometry.mod->uniqueIndices[i]]);
				//std::cout << "dist is " << dist << std::endl;
				if (dist < -0.0000000001f) {
					glm::vec3 p = rigid_it->geometry.mod->vertices[rigid_it->geometry.mod->uniqueIndices[i]];
					//register the contact
					contacts.push_back(Contact(&(*rigid_it), &(*plane_it), p, rigid_it->geometry.mod->uniqueIndices[i]));
					rigid_it->obb.inst.mat->color = glm::vec4(1, 0, 0,1);
				}
			}
		}
	}
	if (contacts.size() == 0)
		return;
	//sort the contacts on the rigidbody's position
	std::sort(contacts.begin(), contacts.end(), [](auto i, auto j) {
		if (i.rigid->position.x == j.rigid->position.x){ 
			if (i.rigid->position.y == j.rigid->position.y) { 
				return i.rigid->position.z < j.rigid->position.z;
			}
			return i.rigid->position.y < j.rigid->position.y;
		}
		return i.rigid->position.x < j.rigid->position.x;
	});
	//find equal ranges iteratively
	std::pair<std::vector<Contact>::iterator, std::vector<Contact>::iterator> bounds;
	bounds.first = contacts.begin();
	bounds.second = contacts.begin();
	do{
		bounds = std::equal_range(bounds.second, contacts.end(), *bounds.second, [](auto i,auto j){
			if (i.rigid->position.x == j.rigid->position.x) {
				if (i.rigid->position.y == j.rigid->position.y) {
					return i.rigid->position.z < j.rigid->position.z;
				}
				return i.rigid->position.y < j.rigid->position.y;
			}
			return i.rigid->position.x < j.rigid->position.x;
		});
		//find the min point of the collisions in the range, in the plane normal direction
		glm::vec3 minPoint = glm::vec3((std::numeric_limits<float>::max)());
		float minDistance = (std::numeric_limits<float>::max)();
		float dist = 0.0;
		for (std::vector<Contact>::iterator it = bounds.first; it != bounds.second; it++) {
			if ((dist=distance::point2plane(it->plane->position, it->plane->normal, it->point))<minDistance) {
				minDistance = dist;
				minPoint = it->point;
			}
		}
		//accumulate the point that have the same value in the normal direction with the min point
		glm::vec3 meanPoint = glm::vec3(0.0f);
		int mean_num = 0;
		for (std::vector<Contact>::iterator it = bounds.first; it != bounds.second; it++) {
			if (distance::point2plane(it->plane->position,it->plane->normal,it->point) == minDistance) {
				meanPoint += it->point;
				mean_num++;
			}
		}
		if (mean_num > 0) {
			meanPoint = meanPoint / ((float)mean_num);
		}
		//calculate the point of penetration to the plane
		glm::vec3 pa= (bounds.first->rigid->linearMomentum / bounds.first->rigid->mass+ glm::cross(glm::inverse(bounds.first->rigid->inertiaTensor)*bounds.first->rigid->angularMomentum, -(meanPoint-bounds.first->rigid->position)));
		float nnDist = glm::abs(distance::point2plane(bounds.first->plane->position, bounds.first->plane->normal, meanPoint));
		float h = nnDist / (glm::dot(glm::normalize(bounds.first->plane->normal), -glm::normalize(pa)));
		glm::vec3 penPoint = meanPoint - h*glm::normalize(pa);
		glm::vec3 offset = penPoint - meanPoint;
		//move the rigidbody position and contact point by the offset
		if(glm::length(offset)>0.0001)
		if (!(offset.x != offset.x || offset.y != offset.y || offset.z != offset.z)) {
			bounds.first->rigid->position += offset;
			meanPoint += offset;
		}
		//store the fixed contact
		finalContacts.push_back(Contact(bounds.first->rigid,bounds.first->plane,meanPoint,0));
	} while (bounds.second != contacts.end());

	//replace contacts with fixed contacts
	contacts = std::vector<Contact>(finalContacts);
}

void RigidbodySystem::HandleContacts(float deltaTime) {
	float epsilon = 0.01f;
	//iterate through each contact
	for (std::vector<Contact>::iterator cont_it = contacts.begin(); cont_it != contacts.end(); cont_it++) {
		//calculate impulse magnitude
		glm::vec3 ra = -(cont_it->point - cont_it->rigid->position);
		glm::vec3 n = glm::normalize(cont_it->plane->normal);
		glm::vec3 pa_rate = (cont_it->rigid->linearMomentum / cont_it->rigid->mass)
			+ glm::cross(glm::inverse(cont_it->rigid->inertiaTensor)*cont_it->rigid->angularMomentum, ra);
		glm::vec3 pb_rate = glm::vec3(0, 0, 0);
		float v_rel = glm::dot(n, pa_rate - pb_rate);
		float N = -(1 + cont_it->plane->restitution_coef)*(v_rel);
		float t1 = 1.0f / cont_it->rigid->mass;
		float t2 = 0;//infinite mass of the plane
		float t3 = glm::dot(n, glm::cross(glm::inverse(cont_it->rigid->inertiaTensor)*glm::cross(ra, n), ra));
		float t4 = 0.0f;
		//t4 shall be zero
		float impulseMagnitude = N / (t1 + t2 + t3 + t4);
		if (v_rel >= -epsilon){
			
			if (v_rel < -epsilon*0.5){
				cont_it->rigid->linearMomentum = glm::vec3(0.0f);
				cont_it->rigid->angularMomentum = glm::vec3(0.0f);

			}
			continue;
		}
		
		glm::vec3 impulse = impulseMagnitude*n;
		cont_it->rigid->linearMomentum += impulse;
		cont_it->rigid->angularMomentum += glm::cross(ra, impulse);
	}
}

void RigidbodySystem::Render() {
	for (std::vector<Rigidbody>::iterator itt = rigidBodies.begin(); itt != rigidBodies.end()-1; itt++) {
		itt->Render(lineMat,false);
	}
	(rigidBodies.end() - 1)->Render(lineMat, true);
	for (std::vector<Plane>::iterator it = planes.begin(); it != planes.end(); it++) {
		it->Render();
	}
}

void RigidbodySystem::RenderDebug() {
	for (std::vector<Rigidbody>::iterator itt = rigidBodies.begin(); itt != rigidBodies.end(); itt++) {

		itt->RenderDebug();
	}
}