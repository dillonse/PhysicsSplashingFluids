
//fan model from https://www.cgtrader.com/free-3d-models/exterior/industrial/industrial-fan

#include <Windows.h>
#include "debug.h"
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <random>
#include <GL/glew.h>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/matrix_decompose.hpp>
#include <glm/gtc/constants.hpp>
#include "textfile.h"
#include "AuxilaryIO.h"
#include "VBO.h"
#include "textures.h"
#include "shaders.h"
#include "model.h"
#include "instance.h"
#include <string>
#include <tuple>
#include <algorithm>
#include "global.h"
//Physics
#include "Rigidbody.h"
#include "RigidbodySystem.h"
#include "ParticleSystem.h"
#include "Contact.h"
#include "VolumeModel.h"
#include "SurfaceModel.h"
//particle systems
ParticleSystem particleSystem;
std::vector<instance> colliders;
//rigidbodies
RigidbodySystem rigidbodySystem;
Rigidbody* rigidbody;

//random generators
//std::default_random_engine generatorr;
//std::uniform_int_distribution<int> distributionn(-9, 9);

//models
model skybox_mod;
model volume_mod;
model particle_mod;
model object_mod;
//model chamber_mod;
//model windows_mod;
//model ball_mod;
//model bbox_mod;
//model fan_mod;
//model fan_rot_mod;
//model rigidbody_mod;
//model magnet_mod;
//model sphere_mod;
//model obb_mod;
//model plane_mod;
//materials
material skybox_mat;
material volume_mat;
material particle_mat;
material object_mat;
//material chamber_mat;
//material windows_mat;
//material ball_mat;
//material bbox_mat;
//material fan_mat;
//material fan_rot_mat;
//material rigidbody_mat;
//material magnet_mat;
//material font_mat;
//material sphere_mat;
//material obb_mat;
//material floor_mat;

//instances
instance skybox_inst;
instance volume_inst;
instance particle_inst;
instance object_inst;

//volume model
VolumeModel volumeModel;
//surface model
SurfaceModel surfaceModel;

//demo variables
int cameraRotationDirection = 1;
float cameraRotationSpeed = 50.0f;
int cameraZoomDirection = 1;
float cameraZoomSpeed=50.0f;

bool pause = true;

float lightSpeed = 0.05f;
glm::vec3 lightMovementAmplitude(5,15,15);
float sphereRotationSpeed = 50;

int numParticles = 5000;
float fan_strength_left = 0.0f;
float fan_strength_right = 0.0f;
float lifetime=100;

float avgSize = 2;

glm::vec3 rotationVector = glm::vec3(0, 0, 0);

int wall_height= 2;
int wall_width = 2;
float explosionForce = 1000.0f;
float fragment_distance = 0.5f;
glm::vec3 explosionPos = glm::vec3(-0.25f,-0.25f,1.0f);

instance collider_cube;
instance  collidee_cube;

glm::vec3 floor_normal = glm::vec3(0, 1, 0);

float navigationSpeed = 1.0f;
float simulationSpeed = 0.1f;

bool gravityOn = false;

float restit_coeff = 0.5;

void printtext(int x, int y, std::string String)
{	
	//(x,y) is from the bottom left of the window
	glDisable(GL_DEPTH_TEST);
	glUseProgram(0);
	//font_mat.BindTextures();
	//font_mat.BindValues();
	//glm::mat4 projection = glm::ortho(-1, 1, -1, 1);
	//GLuint PLocation = glGetUniformLocation(font_mat.ShaderID, "P");

	//glUniformMatrix4fv(PLocation, 1, GL_FALSE,&projection[0][0]);
	glm::mat4 inverseV = glm::inverse(camera::ViewMatrix);
	glm::mat4 inverseP = glm::inverse(camera::ProjectionMatrix);
	glm::vec3 text_pos = glm::vec4(0.5, 0.5, -1, 0); //inverseV*inverseP*glm::vec4(1,1,-1,1);
//	glLoadIdentity();
//	std::cout << glm::to_string(text_pos) << std::endl;
	//glRasterPos2f(text_pos.x,text_pos.y);
	glRasterPos3f(text_pos.x, text_pos.y,-1);
	for (int i = 0; i<String.size(); i++)
	{
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, String[i]);
	}
}

void initPhysics() {
	volumeModel=VolumeModel(volume_inst,36,1.0f,1.0f);
	surfaceModel = SurfaceModel(volumeModel);
	particle_inst.transform = glm::scale(glm::mat4(1.0f), glm::vec3(0.1, 0.1, 0.1));
	particleSystem.particleInstance = particle_inst;
	particleSystem.unaryForces.push_back(new GravityForce());
	object_inst.mod = new model(object_mod);
	object_inst.mat = &object_mat;
	rigidbody = new Rigidbody(object_inst, glm::vec3(5, 5, 5), object_inst, object_inst);
	rigidbodySystem.rigidBodies.push_back(*rigidbody);
	rigidbodySystem.unaryForces.push_back(new GravityForce());
	rigidbodySystem.UpdateGeometry(time::deltaTime);
}

void updatePhysics() {
	time::currentFrameTime = glutGet(GLUT_ELAPSED_TIME);
	time::pastDeltaTime = time::deltaTime;
	time::deltaTime = ((float)(time::currentFrameTime - time::previousFrameTime)) / 1000.0;
	//time::deltaTime *= 0.5f;
	time::previousFrameTime = time::currentFrameTime;
	if (!pause) {
		volumeModel.updateColumnVolumes();
		surfaceModel.updateHeights();
		surfaceModel.updateExternalPressures();
		surfaceModel.updateVerticalVelocities();
		surfaceModel.updateHorizontalVelocities();
		surfaceModel.updateParticleSystem(particleSystem);
		surfaceModel.updateCollisions(rigidbodySystem);
		std::vector<instance> inst;
		particleSystem.Update(0.01f,inst);
		rigidbodySystem.UpdateForcesTorque(time::deltaTime);
		rigidbodySystem.UpdateMomentums(time::deltaTime);
		rigidbodySystem.UpdateAuxilary(time::deltaTime);
		rigidbodySystem.UpdateGeometry(time::deltaTime);
		rigidbodySystem.UpdateBoundingVolumes(time::deltaTime);
		//message += "\n"+std::to_string(volumeModel.columns[0][0].volume)+"\n";
		//message += std::to_string(volumeModel.columns[1][0].volume) + "\n";
		//message += std::to_string(volumeModel.columns[2][0].volume) + "\n";
		//volumeModel.printVolumes();
	}
}

void init() {
	//enable backface culling
	glFrontFace(GL_CCW);
	//glCullFace(GL_BACK);
	//glEnable(GL_CULL_FACE);
	//set depth and transparency
	glEnable(GL_DEPTH_TEST);
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//set transformations
	camera::ViewMatrix = glm::lookAt(   // View matrix
		glm::vec3(0, 0, 0), // Camera is at (4,3,3), in World Space
		glm::vec3(0, 0, 1), // and looks at the origin
		glm::vec3(0, 1, 0)  // Head is up (set to 0,-1,0 to look upside-down)
	);

	//lights::light0.position = glm::vec3(15, 2, 16);
	//lights::light0.BindShadowMap();
}

void changeSize(int w, int h) {
	//return;
	std::cout << "w,h" << w << "," << h << std::endl;
	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if (h == 0)
		h = 1;
	camera::ratio = 1.0* w / h;
	// Reset the coordinate system before modifying
	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);
	// Set the correct perspective.
	camera::ProjectionMatrix = glm::perspective(glm::radians(camera::FOV), camera::ratio, 0.1f, 100.0f); //Projection matrix
	viewport::width = w;
	viewport::height = h;
}


void handleControls() {
	if (controls::Key_A || controls::Key_D) {
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, -camera::position);
		camera::ViewMatrix = glm::rotate(camera::ViewMatrix, glm::radians(cameraRotationDirection*time::deltaTime*navigationSpeed*cameraRotationSpeed), glm::vec3(0, 1, 0));
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, camera::position);

		camera::ViewMatrixNoTranslation = glm::rotate(camera::ViewMatrixNoTranslation, glm::radians(cameraRotationDirection*time::deltaTime*navigationSpeed*cameraRotationSpeed), glm::vec3(0, 1, 0));
	}
	if (controls::Key_L) {
		camera::position += glm::vec3(time::deltaTime*navigationSpeed, 0, 0);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(time::deltaTime*navigationSpeed, 0, 0));
	}
	if (controls::Key_J) {
		camera::position += glm::vec3(-time::deltaTime*navigationSpeed, 0, 0);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(-time::deltaTime*navigationSpeed, 0, 0));
	}
	if (controls::Key_I) {
		camera::position += glm::vec3(0, 0, time::deltaTime*navigationSpeed);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0, 0, time::deltaTime*navigationSpeed));
	}
	if (controls::Key_K) {
		camera::position += glm::vec3(0, 0, -time::deltaTime*navigationSpeed);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0, 0, -time::deltaTime*navigationSpeed));
	}
	if (controls::Key_U) {
		camera::position += glm::vec3(0, time::deltaTime*navigationSpeed, 0);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0, time::deltaTime*navigationSpeed, 0));
	}
	if (controls::Key_O) {
		camera::position += glm::vec3(0, -time::deltaTime*navigationSpeed, 0);
		camera::ViewMatrix = glm::translate(camera::ViewMatrix, glm::vec3(0, -time::deltaTime*navigationSpeed, 0));
	}
	if (controls::Key_W || controls::Key_S) {
		camera::FOV += time::deltaTime*navigationSpeed*cameraZoomDirection*cameraZoomSpeed;
		camera::ProjectionMatrix = glm::perspective(glm::radians(camera::FOV), camera::ratio, 0.1f, 100.0f); //Projection matrix
	}
	if (controls::Key_1) {
		surfaceModel.forces[0][0] = 100.0f;
	}
	else {
		surfaceModel.forces[0][0] = 0.0f;
	}
}

void renderScene(void) {
	if(!pause)
		message.clear();
	handleControls();
	updatePhysics();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	//render the skybox
	glDepthMask(GL_FALSE);
	skybox_inst.Render();
	glDepthMask(GL_TRUE);
	glPolygonMode(GL_FRONT,GL_LINE);
	glPolygonMode(GL_FRONT, GL_FILL);
	//volumeModel.Render();
	surfaceModel.Render(*volume_inst.mat);
	particleSystem.Render();
	rigidbodySystem.Render();
	printScreen();
	glutSwapBuffers();
}

void changeSkybox(std::string skyboxPath) {
	std::vector<std::string> skyboxPaths;
	skyboxPaths.push_back(std::string(skyboxPath).append("posx.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negx.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("posy.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negy.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("posz.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negz.jpg"));
	skybox_mat.SkyboxMapID = textures::loadCubemap(skyboxPaths);

}

void processNormalKeys(unsigned char key, int x, int y) {
	std::string skyboxPath;
	std::vector<std::string> skyboxPaths;
	static float zoom = 45.0;
	
	//gravityOn = !gravityOn;
	GravityForce* newGravity = NULL;
	switch (key)
	{
	case ' ':
		pause = !pause;
		std::cout << "pause is " << std::to_string(pause) << std::endl;
		break;
	case 27:
		exit(0);
	case 'a':
		controls::Key_A = true;
		cameraRotationDirection = -1;
		break;
	case 'd':
		controls::Key_D = true;
		cameraRotationDirection = 1;
		break;
	case 'w':
		controls::Key_W = true;
		cameraZoomDirection = 1;
		break;
	case 's':
		controls::Key_S = true;
		cameraZoomDirection = -1;
		break;
	case 'i':
		controls::Key_I = true;
		break;
	case 'j':
		controls::Key_J = true;
		break;
	case 'k':
		controls::Key_K = true;
		break;
	case 'l':
		controls::Key_L = true;
		break;
	case 'o':
		controls::Key_O = true;
		break;
	case 'u':
		controls::Key_U = true;
		break;
	case '0':
		controls::Key_0 = true;
		break;
	case '2':
		controls::Key_2 = true;
		break;
	case '4':
		controls::Key_4 = true;
		break;
	case '5':
		controls::Key_5 = true;
		break;
	case '6':
		controls::Key_6 = true;
		break;
	case '8':
		controls::Key_8 = true;
		break;
	case '7':
		controls::Key_7 = true;
		break;
	case '9':
		controls::Key_9 = true;
		break;
	case '1':
		controls::Key_1 = true;
		break;
	case '3':
		controls::Key_3 = true;
		break;
	case '+':
		controls::Key_plus = true;
		break;
	case '-':
		controls::Key_minus = true;
		break;
	case 'g':
		gravityOn = !gravityOn;
		newGravity = new GravityForce();
		if (!gravityOn) {
			newGravity->gravityAcceleration = glm::vec3(0.0f);
		}
		else {
			newGravity->gravityAcceleration = glm::vec3(0, -1, 0)*9.8f;
		}
		delete rigidbodySystem.unaryForces[5];
		rigidbodySystem.unaryForces[5] = newGravity;
		controls::Key_G = true;
		break;
	case '*':
		controls::Key_times = true;
		break;
	case '/':
		controls::Key_divide = true;
		break;
	default:
		break;
	}
	glutPostRedisplay();
}


void ProcessNormalKeysUp(unsigned char key, int x, int y) {
	switch (key) {
	case 'a':
		controls::Key_A = false;
		break;
	case 'd':
		controls::Key_D = false;
		break;
	case 'w':
		controls::Key_W = false;
		break;
	case 's':
		controls::Key_S = false;
		break;
	case 'i':
		controls::Key_I = false;
		break;
	case 'j':
		controls::Key_J = false;
		break;
	case 'k':
		controls::Key_K = false;
		break;
	case 'l':
		controls::Key_L = false;
		break;
	case 'o':
		controls::Key_O = false;
		break;
	case 'u' :
		controls::Key_U = false;
		break;
	case '0':
		controls::Key_0 = false;
		break;
	case '2':
		controls::Key_2 = false;
		break;
	case '4':
		controls::Key_4 = false;
		break;
	case '5':
		controls::Key_5 = false;
		break;
	case '6':
		controls::Key_6 = false;
		break;
	case '8':
		controls::Key_8 = false;
		break;
	case '7':
		controls::Key_7 = false;
		break;
	case '9':
		controls::Key_9 = false;
		break;
	case '1':
		controls::Key_1 = false;
		break;
	case '3':
		controls::Key_3 = false;
		break;
	case '+':
		controls::Key_plus = false;
		break;
	case '-':
		controls::Key_minus = false;
		break;
	case 'g':
		controls::Key_G = false;
		break;
	case '*':
		controls::Key_times = false;
		break;
	case '/':
		controls::Key_divide = false;
		break;
	}

}

void processSpecialKeys(int key, int x, int y) {
	switch (key)
	{
	case GLUT_KEY_RIGHT:
		controls::Key_Right = true;
		//chamber_mat.choice++;
		break;
	case GLUT_KEY_LEFT:
		controls::Key_Left = true;
		//chamber_mat.choice--;
		break;
	default:
		break;
	}
	glutPostRedisplay();
}

void processSpecialKeysUp(int key, int x, int y) {
	switch (key)
	{
	case GLUT_KEY_RIGHT:
		controls::Key_Right = false;
		break;
	case GLUT_KEY_LEFT:
		controls::Key_Left = false;
		break;
	default:
		break;
	}
}

void processMouseMotion(int x, int y) {
	//return;
	static int previousX = 0;
	static int previousY = 0;
	glm::vec3 offset=glm::vec3(0,0,0);
	previousX = controls::MousePositionX;
	previousY = controls::MousePositionY;
	if (controls::MouseRight||controls::MouseLeft) {
		//calculate the x interval and the y interval
		int deltaX = x - previousX;
		int deltaY = y - previousY;
		int epsilon = 10;
		if (controls::MouseRight) {
			if (deltaX > epsilon) {
			}
			else if (deltaX < epsilon) {
			}
			if (deltaY > epsilon) {
			}
			else if (deltaY < epsilon) {
			}
		}
		else {
			if (deltaX > epsilon) {
			}
			else if (deltaX < epsilon) {
			}
			if (deltaY > epsilon) {
			}
			else if (deltaY < epsilon) {
			}
		}
	}
	controls::MousePositionX = x;
	controls::MousePositionY = y;
}

void ProcessMousePassiveMotion(int x, int y) {
	controls::MousePositionX = x;
	controls::MousePositionY = y;
}

void ProcessMouseWheel(int wheel, int direction, int x, int y) {
	float wheelspeed = 10.0f;
	if (direction != 0) {
	}
}

void processMouseClick(int button, int state, int x, int y) {
	if (button == GLUT_LEFT_BUTTON) {
		if (state == GLUT_DOWN)
			controls::MouseLeft = true;
		else if (state == GLUT_UP)
			controls::MouseLeft = false;
	}
	if (button == GLUT_RIGHT_BUTTON) {
		if (state == GLUT_DOWN)
			controls::MouseRight = true;
		else if (state == GLUT_UP)
			controls::MouseRight = false;
	}
}


void loadShaders() {
	//create a list of the shader paths
	std::vector<std::tuple<std::string, std::string>> shaderPaths;
	std::vector<GLuint> shaderIDs;
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/DebugShader/debug.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/DebugShader/debug.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/SkyboxShader/skybox.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/SkyboxShader/skybox.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/NormalMapShader/normalMap.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/NormalMapShader/normalMap.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/FresnelShader/fresnel.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/FresnelShader/fresnel.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/TextureShader/texture.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/TextureShader/texture.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/TextureNormalMapShader/texnormal.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/TextureNormalMapShader/texnormal.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/ReflectionShader/reflection.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/ReflectionShader/reflection.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/RefractionShader/refraction.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/RefractionShader/refraction.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/ReflectionNormalMapShader/reflnormal.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/ReflectionNormalMapShader/reflnormal.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/RefractionNormalMapShader/refrnormal.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/RefractionNormalMapShader/refrnormal.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/FresnelNormalMapShader/fresrnormal.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/FresnelNormalMapShader/fresrnormal.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/DepthMapShader/depth.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/DepthMapShader/depth.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/DepthMapShader/debugDepthMap.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/DepthMapShader/debugDepthMap.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/ShadowReceiverShader/shadow.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/ShadowReceiverShader/shadow.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/TextureNormalMapLightShader/texnormallight.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/TextureNormalMapLightShader/texnormallight.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/CollidersShader/collider.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/CollidersShader/collider.frag"));
	shaderPaths.push_back(std::make_tuple(
		"D:/PhysicsSplashingFluids/OpenGL(Physics)/OpenGLTemplate/shaders/ParticleShaderColors/particleColor.vert",
		"D:/PhysicsSplashingFluids/OpenGL(Physics)/OpenGLTemplate/shaders/ParticleShaderColors/particleColor.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/FeatureHighlightShader/fhigh.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/FeatureHighlightShader/fhigh.frag"));
	shaderPaths.push_back(std::make_tuple(
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/FontShader/font.vert",
		"C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/shaders/FontShader/font.frag"));

	//load the shaders
	shaders::loadShaders(shaderPaths, shaderIDs);
	//assign to global variables
	skybox_mat.ShaderID = shaderIDs[1];
	volume_mat.ShaderID = shaderIDs[4];
	volume_mat.color = glm::vec4(1, 0, 0,0);
	particle_mat.ShaderID = shaderIDs[16];
	object_mat.ShaderID = shaderIDs[0];
	//particle_mat.color = glm::vec4(0, 0,1.0f, 1);
}

void loadTexture() {
	//load the skybox maps
	std::string skyboxPath = "C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/textures/skybox1/";
	std::vector<std::string> skyboxPaths;
	skyboxPaths.push_back(std::string(skyboxPath).append("posx.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negx.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("posy.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negy.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("posz.jpg"));
	skyboxPaths.push_back(std::string(skyboxPath).append("negz.jpg"));
	skybox_mat.SkyboxMapID = textures::loadCubemap(skyboxPaths);
	//load the volume diff map
	volume_mat.DiffuseMapID = textures::loadTextureDDS("D:/PhysicsSplashingFluids/OpenGL(Physics)/OpenGLTemplate/textures/water.jpg");
}

void loadObject() {
	//load the skybox cube
	if (!loadOBJ("C:/Users/dillonse/Documents/Visual Studio 2015/Projects/OpenGL(Physics)/OpenGLTemplate/models/skybox.obj", skybox_mod.vertices, skybox_mod.normals, skybox_mod.UVs))
		exit(-1);
	skybox_mod.flipFaceOrientation();
	buffers::Load(skybox_mod);
	//load the volume model
	if (!loadOBJ("D:/PhysicsSplashingFluids/OpenGL(Physics)/OpenGLTemplate/models/volumeCube.obj", volume_mod.vertices, volume_mod.normals, volume_mod.UVs))
		exit(-1);
	buffers::Load(volume_mod);
	//load the particle model
	if (!loadOBJ("D:/PhysicsSplashingFluids/OpenGL(Physics)/OpenGLTemplate/models/demo_ball.obj", particle_mod.vertices, particle_mod.normals, particle_mod.UVs));
	buffers::Load(particle_mod);
	//load the object model
	if (!loadOBJ("D:/PhysicsSplashingFluids/OpenGL(Physics)/OpenGLTemplate/models/demo_object.obj", object_mod.vertices, object_mod.normals, object_mod.UVs));
	buffers::Load(object_mod);
}



int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(1200, 800);
	glutCreateWindow("GLSL Example");

	glutDisplayFunc(renderScene);
	glutIdleFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutKeyboardFunc(processNormalKeys);
	glutKeyboardUpFunc(ProcessNormalKeysUp);
	glutSpecialFunc(processSpecialKeys);
	glutSpecialUpFunc(processSpecialKeysUp);
	glutMotionFunc(processMouseMotion);
	glutMouseFunc(processMouseClick);
	glutPassiveMotionFunc(ProcessMousePassiveMotion);
	glutMouseWheelFunc(ProcessMouseWheel);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glewInit();
	init();

	if (glewIsSupported("GL_VERSION_4_5"))
		printf("Ready for OpenGL 4.5\n");
	else {
		printf("OpenGL 4.5 not supported\n");
		//exit(1);
	}
	//load the auxilary data to memory and pass them to GPU
	std::cout << "loading shaders" << std::endl;
	loadShaders();
	loadObject();
	loadTexture();
	//connect the materials and models to instances
	///skybox
	skybox_inst.mod = &skybox_mod;
	skybox_inst.mat = &skybox_mat;
	///volume model
	volume_inst.mod = &volume_mod;
	volume_inst.mat = &volume_mat;
	///particle model
	particle_inst.mod = &particle_mod;
	particle_inst.mat = &particle_mat;
	camera::ViewMatrix = glm::rotate(camera::ViewMatrix, 79.0f, glm::vec3(0, 1, 0));

	initPhysics();
	glutMainLoop();

	return 0;
}

