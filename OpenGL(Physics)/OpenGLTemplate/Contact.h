#pragma once
#include "Rigidbody.h"
#include "Plane.h"

typedef struct Contact {
	Rigidbody* rigid=NULL;
	Plane* plane=NULL;
	glm::vec3 point;
	int rigid_index;
	Contact(Rigidbody* r, Plane* pl, glm::vec3 p, int index) :rigid(r), plane(pl), point(p), rigid_index(index) {
		;
	};
	Contact() { ; };
}Contact;
