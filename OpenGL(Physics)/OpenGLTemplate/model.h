#pragma once
#include <glm/glm.hpp>
#include <vector>
#include "material.h"
struct model {
	//data buffers
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> colors;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> UVs;
	std::vector<glm::vec3> tangents;
	std::vector<glm::vec3> bitangents;
	std::vector<glm::uvec3> indices;
	std::vector<int> uniqueIndices;
	std::vector<float> actives;
	//data points
	glm::vec3 centroid=glm::vec3(0);
	//data memory flags
	GLuint verticesID=-1;
	GLuint normalsID=-1;
	GLuint UVsID=-1;
	GLuint tangentsID=-1;
	GLuint bitangetsID=-1;
	GLuint colorsID = -1;
	GLuint indicesID = -1;
	GLuint activesID = -1;
	void computeTangentBasis();
	void computeCentroid();
	void flipFaceOrientation();
	void deduplicateVertices();
	void calculateUniqueIndices();
	void translate(glm::vec3 translate);
	void rotate(glm::vec3 rotation);
};
