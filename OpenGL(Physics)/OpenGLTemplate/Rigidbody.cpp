#include "Rigidbody.h"
#include "VBO.h"
#include <iostream>
#include <glm\gtx\string_cast.hpp>
#include <limits>
#include "global.h"
#include "Collider.h"
#include "debug.h"
#include <string>
#include <glm\gtx\transform.hpp>

//resource for volume calculation http://stackoverflow.com/questions/1406029/how-to-calculate-the-volume-of-a-3d-mesh-object-the-surface-of-which-is-made-up
//Intetia tensor calculation resource http://www.melax.com/volint

glm::mat3 orthonormalize(glm::mat3& matrix) {
	matrix[0] = matrix[0] / glm::length(matrix[0]);
	matrix[1] = glm::cross(matrix[2], matrix[0]);
	matrix[1] = matrix[1] / glm::length(matrix[1]);
	matrix[2] = glm::cross(matrix[0], matrix[1]);
	matrix[2] = matrix[2] / glm::length(matrix[2]);
	return matrix;
}

glm::mat3 star(glm::vec3 a) {
	glm::mat3 mat(glm::vec3(0, -a.z, a.y), glm::vec3(a.z, 0, -a.x), glm::vec3(-a.y, a.x, 0));
	return mat;
}

float Rigidbody::CalculateVolume() {
	// count is the number of triangles (tris) 
	float volume = 0;
	if (bodyspaceGeometry.indices.size() > 0) {
		for (int i = 0; i < bodyspaceGeometry.indices.size(); i++) // for each triangle
		{
			glm::vec3 v1 = bodyspaceGeometry.vertices[bodyspaceGeometry.indices[i].x];
			glm::vec3 v2 = bodyspaceGeometry.vertices[bodyspaceGeometry.indices[i].y];
			glm::vec3 v3 = bodyspaceGeometry.vertices[bodyspaceGeometry.indices[i].z];
			glm::mat3 mat(v1, v2, v3);
			volume += glm::determinant(mat);
		}
	}
	else {
		for (int i = 0; i < bodyspaceGeometry.vertices.size(); i+=3) // for each triangle
		{
			glm::vec3 v1 = bodyspaceGeometry.vertices[i];
			glm::vec3 v2 = bodyspaceGeometry.vertices[i+1];
			glm::vec3 v3 = bodyspaceGeometry.vertices[i+2];
			glm::mat3 mat(v1, v2, v3);
			volume += glm::determinant(mat);
		}
	}
	return volume / 6.0f; // since the determinant give 6 times tetra volume
}
glm::vec3 Rigidbody::CalculateCOM() {
	// count is the number of triangles (tris) 
	glm::vec3 com(0, 0, 0);
	float volume = 0; // actually accumulates the volume*6
	if (bodyspaceGeometry.indices.size() > 0) {
		for (int i = 0; i < bodyspaceGeometry.indices.size(); i++) // for each triangle
		{
			glm::vec3 v1 = bodyspaceGeometry.vertices[bodyspaceGeometry.indices[i].x];
			glm::vec3 v2 = bodyspaceGeometry.vertices[bodyspaceGeometry.indices[i].y];
			glm::vec3 v3 = bodyspaceGeometry.vertices[bodyspaceGeometry.indices[i].z];
			glm::mat3 mat = glm::mat3(v1, v2, v3);
			float vol = glm::determinant(mat);
			com += vol*(v1, v2, v3);
			volume += vol;
		}
	}
	else {
		for (int i = 0; i < bodyspaceGeometry.vertices.size(); i+=3) // for each triangle
		{
			glm::vec3 v1 = bodyspaceGeometry.vertices[i];
			glm::vec3 v2 = bodyspaceGeometry.vertices[i+1];
			glm::vec3 v3 = bodyspaceGeometry.vertices[i+2];
			glm::mat3 mat = glm::mat3(v1, v2, v3);
			float vol = glm::determinant(mat);
			com += vol*(v1, v2, v3);
			volume += vol;
		}

	}
	com /= volume*4.0f;
	return com;

}

glm::mat3 Rigidbody::CalculateInertia() {
	// count is the number of triangles (tris) 
	// The moments are calculated based on the center of rotation com 
	// com if not provided is assumed to be [0,0,0] by default
	// assume mass==1.0 you can multiply by mass later.
	// for improved accuracy the next 3 variables, the determinant d, 
	// and its calculation should be changed to double
	float volume = 0; // technically this variable accumulates the volume times 6
	glm::vec3 diag(0, 0, 0); // accumulate matrix main diagonal integrals [x*x, y*y, z*z]
	glm::vec3 offd(0, 0, 0); // accumulate matrix off-diagonal integrals [y*z, x*z, x*y]
	glm::vec3 com = CalculateCOM();
	//for (int i = 0; i < bodyspaceGeometry.vertices.size(); i+=3) // for each triangle
	if (bodyspaceGeometry.indices.size() > 0) {
		for (int i = 0; i < bodyspaceGeometry.indices.size(); i++)
		{
			// matrix trick for volume calc by taking determinant
			glm::vec3 v1 = bodyspaceGeometry.vertices[bodyspaceGeometry.indices[i].x];
			glm::vec3 v2 = bodyspaceGeometry.vertices[bodyspaceGeometry.indices[i].y];
			glm::vec3 v3 = bodyspaceGeometry.vertices[bodyspaceGeometry.indices[i].z];
			glm::mat3 A(v1 - com, v2 - com, v3 - com);
			// vol of tiny parallelapiped= d * dr * ds * dt 
			// where dr,ds,dt are the 3 partials of my tetral triple integral equasion
			float d = glm::determinant(A);
			volume += d; // add vol of current tetra (note it could be negative - that's ok)
			for (int j = 0; j < 3; j++)
			{
				int j1 = (j + 1) % 3;
				int j2 = (j + 2) % 3;
				// defer the division of diag by 60 and offd by 120 until after loop
				diag[j] += (A[0][j] * A[1][j] + A[1][j] * A[2][j] + A[2][j] * A[0][j] +
					A[0][j] * A[0][j] + A[1][j] * A[1][j] + A[2][j] * A[2][j]) *d;
				offd[j] += (A[0][j1] * A[1][j2] + A[1][j1] * A[2][j2] + A[2][j1] * A[0][j2] +
					A[0][j1] * A[2][j2] + A[1][j1] * A[0][j2] + A[2][j1] * A[1][j2] +
					A[0][j1] * A[0][j2] * 2 + A[1][j1] * A[1][j2] * 2 + A[2][j1] * A[2][j2] * 2) *d;
			}
		}
	}
	else {
		for (int i = 0; i < bodyspaceGeometry.vertices.size(); i+=3)
		{
			// matrix trick for volume calc by taking determinant
			glm::vec3 v1 = bodyspaceGeometry.vertices[i];
			glm::vec3 v2 = bodyspaceGeometry.vertices[i+1];
			glm::vec3 v3 = bodyspaceGeometry.vertices[i+2];
			glm::mat3 A(v1 - com, v2 - com, v3 - com);
			// vol of tiny parallelapiped= d * dr * ds * dt 
			// where dr,ds,dt are the 3 partials of my tetral triple integral equasion
			float d = glm::determinant(A);
			volume += d; // add vol of current tetra (note it could be negative - that's ok)
			for (int j = 0; j < 3; j++)
			{
				int j1 = (j + 1) % 3;
				int j2 = (j + 2) % 3;
				// defer the division of diag by 60 and offd by 120 until after loop
				diag[j] += (A[0][j] * A[1][j] + A[1][j] * A[2][j] + A[2][j] * A[0][j] +
					A[0][j] * A[0][j] + A[1][j] * A[1][j] + A[2][j] * A[2][j]) *d;
				offd[j] += (A[0][j1] * A[1][j2] + A[1][j1] * A[2][j2] + A[2][j1] * A[0][j2] +
					A[0][j1] * A[2][j2] + A[1][j1] * A[0][j2] + A[2][j1] * A[1][j2] +
					A[0][j1] * A[0][j2] * 2 + A[1][j1] * A[1][j2] * 2 + A[2][j1] * A[2][j2] * 2) *d;
			}
		}
	}
	//divide by total volume (vol/6) since density=1/volume
	diag /= volume*(60.0f / 6.0f);
	offd /= volume*(120.0f / 6.0f);
	return glm::mat3(diag.y + diag.z, -offd.z, -offd.y,
		-offd.z, diag.x + diag.z, -offd.x,
		-offd.y, -offd.x, diag.x + diag.y);
}

Rigidbody::Rigidbody(instance& geometry,glm::vec3 position,instance& boundingSphere,instance& obb):obb(obb),bsphere(boundingSphere){
	this->geometry = geometry;
	bodyspaceGeometry = (*this->geometry.mod);
	this->bodyspaceInertia= CalculateInertia()*mass;
	this->forces.resize(geometry.mod->uniqueIndices.size());
	this->position = position;
	this->nearestPoints.resize(this->geometry.mod->vertices.size());
	this->activeIndices.resize(this->geometry.mod->vertices.size());
	std::fill(this->activeIndices.begin(), this->activeIndices.end(), glm::ivec3(glm::ivec3(-1, -1, -1)));
	this->bsphere.Init(geometry);
	this->obb.Init(geometry);
}

void Rigidbody::Update(float deltaTime) {
	//if(!isColliding)
		UpdateMomentums(deltaTime);
	UpdateAuxilary(deltaTime);
	UpdateGeometry();
	UpdateBoundingVolumes();
}

void Rigidbody::UpdateMomentums(float deltaTime) {
	//linear
	linearMomentum += netForce*deltaTime;
	//rotational
	angularMomentum += netTorque*deltaTime;
}

void Rigidbody::UpdateAuxilary(float deltaTime) {
	//lin
	glm::vec3 velocity = linearMomentum / mass;
	position += velocity*deltaTime;
	//rot
	bodyspaceInertia[2][2] = bodyspaceInertia[1][1];
	
	this->inertiaTensor = orientation*bodyspaceInertia*glm::transpose(orientation);
	glm::vec3 angularVelocity = glm::inverse(inertiaTensor)*angularMomentum;
	orientation += star(angularVelocity)*orientation*deltaTime;
	orientation = orthonormalize(orientation);
}

void Rigidbody::UpdateGeometry() {
	for (int i = 0; i < geometry.mod->vertices.size(); i++) {
		geometry.mod->vertices[i] = orientation*bodyspaceGeometry.vertices[i]+ position;
	}
}

void Rigidbody::UpdateBoundingVolumes() {
#if COLLISION == SPHERE
	bsphere.position = bsphere.bodyspacePosition+position;
#elif COLLISION == OBBS
	obb.position = obb.bodyspacePosition + position;
	for (int i = 0; i < obb.faceNormals.size(); i++) {
		obb.faceNormals[i] = orientation*obb.bodyspaceFaceNormals[i];
	}
#elif COLLISION == SPHEREOBB
	bsphere.position = position;
	obb.position = position;
	for (int i = 0; i < obb.faceNormals.size(); i++) {
		obb.faceNormals[i] = orientation* obb.bodyspaceFaceNormals[i];
	}
#endif
}

void Rigidbody::RenderLines(material& mat) {
	glUseProgram(mat.ShaderID);
	mat.color = glm::vec4(1, 0, 0, 1);
	mat.BindValues();
	glm::vec4 original_color = mat.color;
	glm::mat4 MVP = camera::ProjectionMatrix * camera::ViewMatrix;
	GLuint MVPLocation = glGetUniformLocation(mat.ShaderID, "MVP");
	glUniformMatrix4fv(MVPLocation, 1, GL_FALSE, &MVP[0][0]);
	glPointSize(10.0f);
	glBegin(GL_POINTS);
	//glVertex3f(position.x,position.y,position.z);
	glEnd();
	return;//comment for narrow phase demo
	float linewidth;
	glGetFloatv(GL_LINE_WIDTH,&linewidth);
	for (int i = 0; i < nearestPoints.size(); i++) {
		mat.color = original_color;
		mat.BindValues();
		glLineWidth(0.1);
		glBegin(GL_LINES);
		glVertex3f(geometry.mod->vertices[i].x, geometry.mod->vertices[i].y, geometry.mod->vertices[i].z);
		glVertex3f(nearestPoints[i].x, nearestPoints[i].y, nearestPoints[i].z);
		glEnd();
		
		glBegin(GL_POINTS);
		
		glEnd();
		if (activeIndices[i].x == activeIndices[i].y && activeIndices[i].y== activeIndices[i].z) {
			mat.color = glm::vec4(0, 1, 0, 1);
			mat.BindValues();
		}
		glBegin(GL_POINTS);
		glVertex3f(nearestPoints[i].x, nearestPoints[i].y, nearestPoints[i].z);
		glEnd();
	}
	glLineWidth(linewidth);
}

void Rigidbody::Render(material& debugMat,bool lines) {
	buffers::Load(*geometry.mod);
	geometry.Render();
	if(lines)
		RenderLines(debugMat);
}

void Rigidbody::RenderDebug() {
#if COLLISION == SPHERE
	bsphere.Render();
#elif COLLISION == OBBS
	obb.Render(orientation);
#elif COLLISION == SPHEREOBB
	bsphere.Render();
	obb.Render(orientation);
#endif
}
	

void Rigidbody::ClearNearestPoints() {
	for (int i = 0; i < nearestPoints.size(); i++) {
		//nearestPoints[i] = glm::vec3(std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max());
	}
}

void Rigidbody::ClearActives() {
	std::fill(this->geometry.mod->actives.begin(), this->geometry.mod->actives.end(),0);
}

void Rigidbody::FindNearestPoints(Rigidbody& collidee) {

	for (int i = 0; i < nearestPoints.size(); i++) {
		glm::vec3 p0 = this->geometry.mod->vertices[i];
		//iterate through each triangle of the collidee
		float distance = 0.0f;
		for (int j = 0; j < collidee.geometry.mod->vertices.size(); j += 3) {
			glm::vec3 p1 = collidee.geometry.mod->vertices[j];
			glm::vec3 p2 = collidee.geometry.mod->vertices[j + 1];
			glm::vec3 p3 = collidee.geometry.mod->vertices[j + 2];
			//find the nearest point of triangle p1p2p3 to the point p0
			nearest::FEATURE feat;
			glm::vec3 nn=nearest::point2Triangle(p0, p1, p2, p3,feat);
			nearestPoints[i] = nn;
			//use the active mask to set the active indices
			std::string collisionFeat = "";
			if (feat == nearest::FEATURE::P1) {
				activeIndices[i] = glm::vec3(j,j,j);
				distance = distance::point2point(p0, p1);
				collisionFeat = "point";
			}
			else if (feat == nearest::FEATURE::P2) {
				activeIndices[i] = glm::vec3(j+1, j+1, j+1);
				distance = distance::point2point(p0, p2);
				collisionFeat = "point";
			}
			else if (feat == nearest::FEATURE::P3) {
				activeIndices[i] = glm::vec3(j + 2, j + 2, j + 2);
				distance = distance::point2point(p0, p3);
				collisionFeat = "point";
			}
			else if (feat == nearest::FEATURE::P12) {
				activeIndices[i] = glm::vec3(j, j + 1, j + 1);
				distance = distance::point2edge(p0, p1, p2);
				collisionFeat = "edge";
			}
			else if (feat == nearest::FEATURE::P23) {
				activeIndices[i] = glm::vec3(j + 1, j + 2, j + 2);
				distance = distance::point2edge(p0, p2, p3);
				collisionFeat = "edge";
			}
			else if (feat == nearest::FEATURE::P31) {
				activeIndices[i] = glm::vec3(j + 2, j, j);
				distance = distance::point2edge(p0, p3, p1);
				collisionFeat = "edge";
			}
			else if (feat == nearest::FEATURE::P123) {
				activeIndices[i] = glm::vec3(j, j + 1, j + 2);
				//calculate the normal of the plane as the avg of the normals of its vertices
				glm::vec3 normal= (collidee.geometry.mod->normals[j]+collidee.geometry.mod->normals[j+1]+collidee.geometry.mod->normals[j+2])*0.33333f;
				normal = glm::normalize(normal);
				distance = distance::point2plane(p0, normal, p1);
				collisionFeat = "triangle";
			}
			message += "\n Distance:point "+glm::to_string(p0)+". Triangle "+glm::to_string(p1)+","+glm::to_string(p2)+","+glm::to_string(p3)+"\n"+std::to_string(distance)+" type:"+collisionFeat;
		}
		collidee.geometry.mod->actives[activeIndices[i].x] = 1.0f;
		collidee.geometry.mod->actives[activeIndices[i].y] = 1.0f;
		collidee.geometry.mod->actives[activeIndices[i].z] = 1.0f;
	}
}