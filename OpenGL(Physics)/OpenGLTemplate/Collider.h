#pragma once
#include <glm/vec3.hpp>
#include "Rigidbody.h"
#include "Plane.h"
namespace distance {
	float point2point(glm::vec3 p1,glm::vec3 p2);
	float point2edge(glm::vec3 p, glm::vec3 p1, glm::vec3 p2);
	float point2plane(glm::vec3 p, glm::vec3 normal,glm::vec3 p1);
};

namespace nearest {
	enum FEATURE {
		P1,P2,P3,P12,P23,P31,P123
	};
	glm::vec3 point2Triangle(glm::vec3 p, glm::vec3 p1, glm::vec3 p2, glm::vec3 p3,FEATURE& feat);
	glm::vec3 point2Plane(glm::vec3 p, Plane plane);
};

namespace detection {
	bool Sphere(Rigidbody& r1, Rigidbody& r2);
	bool OBB(Rigidbody& r1, Rigidbody& r2);
}