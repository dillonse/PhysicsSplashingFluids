#pragma once
#include "model.h"
#include "material.h"
struct instance {
	std::string name;
	model* mod=NULL;
	model* collider = NULL;
	material * mat = NULL;
	glm::vec3 position;
	glm::mat4 transform = glm::mat4(1.0);
	glm::mat4 rotate(GLfloat rotX,GLfloat rotY,GLfloat rotZ);
	glm::mat4 scale(GLfloat scaleX, GLfloat scaleY, GLfloat scaleZ);
	glm::mat4 rotateAround(GLfloat rotX, GLfloat rotY, GLfloat rotZ,glm::vec3 pivot);
	glm::mat4 translate(GLfloat transX, GLfloat transY, GLfloat transZ);
	glm::mat4 setPosition(glm::vec3 position);
	glm::mat4 setOrientation(glm::mat3 rotation);
	void BindMaterial();
	void BindModel();
	void Render();
	void RenderWithoutMaterials();
};