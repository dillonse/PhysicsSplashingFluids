#ifndef  _GLOBAL_H
#define _GLOBAL_H

#include <glm/matrix.hpp>
#include <GL\glew.h>
#include "light.h"
namespace camera{
	extern glm::mat4 ViewMatrix;
	extern glm::mat4 ProjectionMatrix;
	extern GLfloat ratio;
	extern GLfloat FOV;
	extern glm::vec3 position;
	extern glm::mat4 ViewMatrixNoTranslation;
}

namespace viewport {
	extern int width;
	extern int height;
}

namespace lights {
	 extern light light0;
	 extern light light1;
}

namespace controls {
	//alpharithmetics
	extern GLboolean Key_A;
	extern GLboolean Key_D;
	extern GLboolean Key_W;
	extern GLboolean Key_S;
	extern GLboolean Key_I;
	extern GLboolean Key_K;
	extern GLboolean Key_J;
	extern GLboolean Key_L;
	extern GLboolean Key_U;
	extern GLboolean Key_O;
	extern GLboolean Key_0;
	extern GLboolean Key_5;
	extern GLboolean Key_8;
	extern GLboolean Key_2;
	extern GLboolean Key_4;
	extern GLboolean Key_6;
	extern GLboolean Key_7;
	extern GLboolean Key_9;
	extern GLboolean Key_1;
	extern GLboolean Key_3;
	extern GLboolean Key_plus;
	extern GLboolean Key_minus;
	extern GLboolean Key_times;
	extern GLboolean Key_divide;
	extern GLboolean Key_G;
	//specials
	extern GLboolean Key_Left;
	extern GLboolean Key_Right;
	//mouse
	extern GLboolean MouseRight;
	extern GLboolean MouseLeft;
	extern GLint MousePositionX;
	extern GLint MousePositionY;
}

namespace time {
	extern GLfloat previousFrameTime;
	extern GLfloat currentFrameTime;
	extern GLfloat deltaTime;
	extern GLfloat pastDeltaTime;
}

#endif