#include "ParticleSystem.h"
#include "PhysicsUtilities.h"
#include <iostream>
#include <glm\gtx\string_cast.hpp>
#include <random>

#define IMPULSE 0
#define FORCE 1 
#define POST 2
#define RESPONSE POST

std::default_random_engine generator;
std::uniform_int_distribution<int> distribution(0, 9);
void ParticleSystem::Update(float deltaTime,std::vector<instance>& colliders) {
	//iterate through all the particles
	for (std::deque<Particle>::iterator p = particles.begin(); p != particles.end(); p++) {
		/*Update lifetime*/
		if (p->lifetime <= 0) {
			p->position = p->initialPosition;
			p->velocity = p->initialVelocity;
			p->lifetime = p->initalLifetime;
		}
		/*Update the positions*/
		//iterate through all the forces
		p->force_accumulator = glm::vec3(0);
		for (std::vector<UnaryForce*>::iterator uf = unaryForces.begin(); uf != unaryForces.end(); uf++) {
			//update the force accumulator of the particle
			p->force_accumulator += (*uf)->computeForce(*p);
		}
		//update the particles positions
		glm::vec3 acceleration = p->force_accumulator / p->mass;
		p->velocity += acceleration*deltaTime;
		p->position += p->velocity*deltaTime;
		p->lifetime -= deltaTime;
		/*Visual feedback*/
		
		//iterate over the colliders
#if DEBUG
			for (std::vector<instance>::iterator inst = colliders.begin(); inst != colliders.end(); inst++) {
				//for every third point in the pool
				for (int i = 0; i < inst->mod->colors.size(); i += 3) {
					if (inst->mod->colors[i] == glm::vec3(1, 0, 0)) {
						inst->mod->colors[i] = glm::vec3(1, 1, 0);
						inst->mod->colors[i + 1] = glm::vec3(1, 1, 0);
						inst->mod->colors[i + 2] = glm::vec3(1, 1, 0);
					}
					else if (inst->mod->colors[i] != glm::vec3(1, 1, 0)) {
						inst->mod->colors[i] = glm::vec3(0, 1, 0);
						inst->mod->colors[i + 1] = glm::vec3(0, 1, 0);
						inst->mod->colors[i + 2] = glm::vec3(0, 1, 0);
					}
				}
			}
#endif
		/*Detect Collisions*/
		//iterate over the colliders
		model mod;
		for (std::vector<instance>::iterator inst = colliders.begin(); inst != colliders.end(); inst++) {
			//for every third point in the pool
			for (int i = 0; i < inst->mod->vertices.size(); i += 3) {
				//check for colision using the extreme point of the triangle and the normal 
				if (intersection::isPenetrating(*p,inst->mod->vertices[i],inst->mod->normals[i])) {
				//if (intersection::isPenetrating(*p,inst->mod->vertices[i],inst->mod->normals[i])) {
#if DEBUG
					inst->mod->colors[i] = glm::vec3(1, 0, 0);
					inst->mod->colors[i + 1] = glm::vec3(1, 0, 0);
					inst->mod->colors[i + 2] = glm::vec3(1, 0, 0);
#endif
#if RESPONSE == IMPULSE
					if (distribution(generator) % 2)
						response::impulse(*p, inst->mod->normals[i], inst->mod->tangents[i], 0.5f);
					else
						response::impulse(*p, inst->mod->normals[i], inst->mod->bitangents[i], 0.5f);
					p->position += p->velocity*deltaTime;
					//I notice that there may be situations where this resolution is still a collission. 
					//I could use a second test and recycle if it doesnt pass it.
#elif RESPONSE == FORCE
					glm::vec3 nonpenForce = 10.1f*response::forces(*p, inst->mod->normals[i], 0.5);
					glm::vec3 acceleration = nonpenForce / p->mass;
					p->velocity += acceleration*deltaTime;
					p->position += p->velocity*deltaTime;
#elif RESPONSE == POST
					response::postProcessing(*p, inst->mod->vertices[i], inst->mod->normals[i],1.0f);
#endif
				} 
			}
		}
	}

}

void ParticleSystem::Reset() {
	for (std::deque<Particle>::iterator p = particles.begin(); p != particles.end(); p++) {
		p->position = p->initialPosition;
		p->velocity = p->initialVelocity;
		p->lifetime = p->initalLifetime;
	}
}

void ParticleSystem::Render() {
	//iterate through all the particles
	for (std::deque<Particle>::iterator p = particles.begin(); p != particles.end(); p++) {
		particleInstance.setPosition(p->position);
		particleInstance.RenderWithoutMaterials();
	}
}